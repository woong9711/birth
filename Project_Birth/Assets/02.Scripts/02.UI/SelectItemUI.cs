using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectItemUI : MonoBehaviour
{
    [SerializeField, ReadOnly] private Vector2 RollBackPosition;
    [SerializeField, ReadOnly] private GameObject BackGround;
    
    [SerializeField, ReadOnly] private List<RectTransform> ItemList = new List<RectTransform>(5);
    [SerializeField, ReadOnly] private List<GlobalValue.ItemCode> CurSelectItemList;

    [SerializeField] private List<Sprite> UnSelectedImageList;
    [SerializeField] private List<Sprite> SelectedImageList;

    private GameObject Statue_Bear;
    private GameObject Statue_Human;
    void Start()
    {
        RollBackPosition = GetComponent<RectTransform>().anchoredPosition;
        BackGround = gameObject.transform.GetChild(2).gameObject;

        ItemList = BackGround.GetComponentsInChildren<RectTransform>().ToList();
        ItemList.Remove(BackGround.GetComponent<RectTransform>());

        Statue_Bear = GameObject.Find("Statue_Bear");
        Statue_Human = GameObject.Find("Statue_Human");
        Statue_Human.SetActive(false);


        gameObject.SetActive(false);
    }
    public void OnClickedItem(GameObject obj)
    {
        Editor_ItemElement temp = obj.GetComponent<Editor_ItemElement>();
        Image img = obj.GetComponent<Image>();

        if (temp != null)
        {
            if (CurSelectItemList.Contains(temp.code))
            {
                CurSelectItemList.Remove(temp.code);
                img.color += new Color(0.0f, 0.0f, 0.0f, -0.5f);
            }
            else
            {
                CurSelectItemList.Add(temp.code);
                img.color += new Color(0.0f, 0.0f, 0.0f, 0.5f);
            }
        }

    }
    public void OnOK()
    {

        if (CurSelectItemList.Count == 2)
        {
            if (CurSelectItemList.Contains(GlobalValue.ItemCode.Garlic) && CurSelectItemList.Contains(GlobalValue.ItemCode.Mugwort))
            {
                Statue_Bear.SetActive(false);
                Statue_Human.SetActive(true);

                Player_TriggerReceiver._instance.StatueFlag = true;

                GameObject item1 = Inventory._instance.FindwithCode(GlobalValue.ItemCode.Garlic);
                GameObject item2 = Inventory._instance.FindwithCode(GlobalValue.ItemCode.Mugwort);

                Inventory._instance.RemoveItem(item1);
                Inventory._instance.RemoveItem(item2);
            }
        }

        RollBack();
    }
    public void OnCancel()
    {
        RollBack();
    }
    private void RollBack()
    {
        CurSelectItemList.Clear();
        gameObject.GetComponent<RectTransform>().anchoredPosition = RollBackPosition;
        gameObject.SetActive(false);
        Inventory._instance.gameObject.SetActive(true);
    }

    public void SetItemSprite(List<Image> list)
    {
        for (int i = 0; i < ItemList.Count; i++)
        {
            if(i < list.Count)
                ItemList[i].gameObject.SetActive(true);
            else
                ItemList[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < list.Count; i++)
        {
            ItemList[i].GetComponent<Image>().sprite = list[i].sprite;
            Color temp = ItemList[i].GetComponent<Image>().color;
            temp.a = 0.5f;
            ItemList[i].GetComponent<Image>().color = temp;
            ItemList[i].GetComponent<Editor_ItemElement>().code = list[i].gameObject.GetComponent<Editor_ItemElement>().code;
        }
            
    }
}
