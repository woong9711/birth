using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class UI_Animations
{

    [Tooltip("Load Scene Before")]
    public GlobalValue.Animation_Type _before;

    [Tooltip("Load Scene After")]
    public GlobalValue.Animation_Type _after;

    [Tooltip("어떤 버튼을 눌렀을때 진행되는 애니메이션인지")]
    public GlobalValue.Button_Type _btype;

    [Tooltip("현재 씬에서 어떤 씬으로 이동할 건지")]
    public string SceneName;

}

