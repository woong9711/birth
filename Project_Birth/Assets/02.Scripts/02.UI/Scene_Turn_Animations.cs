using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scene_Turn_Animations : MonoBehaviour
{
    enum type { one, two};
    [Header("Components")]
    [SerializeField] private Image anim_Image;
    [SerializeField, ReadOnly] private RectTransform rectTransform;
    [SerializeField, ReadOnly] private List<Image> CrackAnim_Image_List;
    [SerializeField, ReadOnly] private Image[] Horizontal_Images;
    [SerializeField, ReadOnly] private Image[] Vertical_Images;


    [Header("Datas")]
    [Tooltip("애니메이션 속도 배속")]
    [SerializeField] private float Anim_Speed = 1f;
    [Tooltip("애니메이션 정보")]
    [ReadOnly] public UI_Animations CurAnimation;

    private bool Loadflag;

    void LoadScene()
    {
        if (!Loadflag)
            return;
        SceneManager.LoadScene(CurAnimation.SceneName);
    }

    private IEnumerator Start_Animations()
    {
        GlobalValue.Animation_Type temp = Loadflag ? CurAnimation._before : CurAnimation._after;

        switch (temp)
        {
            case GlobalValue.Animation_Type.NONE:
                yield return null;
                break;
            case GlobalValue.Animation_Type.FadeOn:
                yield return StartCoroutine(Anim_FadeOn());
                break;
            case GlobalValue.Animation_Type.FadeOff:
                yield return StartCoroutine(Anim_FadeOff());
                break;
            case GlobalValue.Animation_Type.LtoC:
                yield return StartCoroutine(Anim_LefttoCenter());
                break;
            case GlobalValue.Animation_Type.RtoC:
                yield return StartCoroutine(Anim_RighttoCenter());
                break;
            case GlobalValue.Animation_Type.UtoC:
                yield return StartCoroutine(Anim_UptoCenter());
                break;
            case GlobalValue.Animation_Type.DtoC:
                yield return StartCoroutine(Anim_DowntoCenter());
                break;
            case GlobalValue.Animation_Type.CtoL:
                yield return StartCoroutine(Anim_CentertoLeft());
                break;
            case GlobalValue.Animation_Type.CtoR:
                yield return StartCoroutine(Anim_CentertoRight());
                break;
            case GlobalValue.Animation_Type.CtoU:
                yield return StartCoroutine(Anim_CentertoUp());
                break;
            case GlobalValue.Animation_Type.CtoD:
                yield return StartCoroutine(Anim_CentertoDown());
                break;
            case GlobalValue.Animation_Type.Crack_RandomFadeOn:
                yield return StartCoroutine(Anim_CrackRandomFadeOn());
                break;
            case GlobalValue.Animation_Type.Crack_RandomFadeOff:
                yield return StartCoroutine(Anim_CrackRandomFadeOff());
                break;
            case GlobalValue.Animation_Type.Crack_FadeOnOrder:
                yield return StartCoroutine(Anim_CrackOrderFadeOn());
                break;
            case GlobalValue.Animation_Type.Crack_FadeOffOrder:
                yield return StartCoroutine(Anim_CrackOrderFadeOff());
                break;
            case GlobalValue.Animation_Type.Crack_ReverseFadeOnOrder:
                yield return StartCoroutine(Anim_CrackReverseOrderFadeOn());
                break;
            case GlobalValue.Animation_Type.Crack_ReverseFadeOffOrder:
                yield return StartCoroutine(Anim_CrackReverseOrderFadeOff());
                break;
            case GlobalValue.Animation_Type.Horizontal_Open:
                yield return StartCoroutine(Anim_HorizontalOpen());
                break;
            case GlobalValue.Animation_Type.Horizontal_Close:
                yield return StartCoroutine(Anim_HorizontalClose());
                break;
            case GlobalValue.Animation_Type.Vertical_Open:
                yield return StartCoroutine(Anim_VerticalOpen());
                break;
            case GlobalValue.Animation_Type.Vertical_Close:
                yield return StartCoroutine(Anim_VerticalClose());
                break;
            default:
                break;
        }

        yield return null;
        LoadScene();
    }

    public void SetUIAnimation(UI_Animations ani, bool flag = true)
    {
        CurAnimation = ani;
        Loadflag = flag;

        StartCoroutine(Start_Animations());
    }

    private void Start()
     {
        rectTransform = anim_Image.GetComponent<RectTransform>();
        Loadflag = false;

        CrackAnim_Image_List = new List<Image>();
        CrackAnim_Image_List = transform.GetChild(1).GetComponentsInChildren<Image>().ToList<Image>();

        Horizontal_Images = new Image[2];
        Horizontal_Images = GameObject.Find("Horizontal_Images").transform.GetComponentsInChildren<Image>();
        Vertical_Images = new Image[2];
        Vertical_Images = GameObject.Find("Vertical_Images").transform.GetComponentsInChildren<Image>();

        DontDestroyOnLoad(this);
        DontDestroyOnLoad(GetComponent<Canvas>().worldCamera);
    }

	#region Animation_Coroutine_Functions
	
    #region Fade
	private IEnumerator Anim_FadeOn()
    {
        yield return StartCoroutine(FadeOnGradual(anim_Image, (1f / Application.targetFrameRate)));
    }

    private IEnumerator Anim_FadeOff()
    {
        yield return StartCoroutine(FadeOffGradual(anim_Image, (1f / Application.targetFrameRate)));
    }
	#endregion

	#region To Center
	private IEnumerator Anim_LefttoCenter()
    {
        yield return StartCoroutine(HorizontalMove_Right(anim_Image, true));
    }
    private IEnumerator Anim_RighttoCenter()
    {
        yield return StartCoroutine(HorizontalMove_Left(anim_Image, true));
    }
    private IEnumerator Anim_UptoCenter()
    {
        yield return StartCoroutine(VerticalMove_Down(anim_Image, true));
    }
    private IEnumerator Anim_DowntoCenter()
    {
        yield return StartCoroutine(VerticalMove_Up(anim_Image, true));
    }
    #endregion

    #region From Center
    private IEnumerator Anim_CentertoLeft()
    {
        yield return StartCoroutine(HorizontalMove_Left(anim_Image, false));
    }
    private IEnumerator Anim_CentertoRight()
    {
        yield return StartCoroutine(HorizontalMove_Right(anim_Image, false));
    }
    private IEnumerator Anim_CentertoUp()
    {
        yield return StartCoroutine(VerticalMove_Up(anim_Image, false));
    }
    private IEnumerator Anim_CentertoDown()
    {
        yield return StartCoroutine(VerticalMove_Down(anim_Image, false));
    }

	#endregion

	#region Tween Move
	
	private IEnumerator Anim_HorizontalOpen()
    {
        StartCoroutine(VerticalMove_Up(Horizontal_Images[0], false));
        yield return StartCoroutine(VerticalMove_Down(Horizontal_Images[1], false));
        yield return null;
    }

    private IEnumerator Anim_HorizontalClose()
    {
        StartCoroutine(VerticalMove_Down(Horizontal_Images[0], true));
        yield return StartCoroutine(VerticalMove_Up(Horizontal_Images[1], true));
        yield return null;
    }

    private IEnumerator Anim_VerticalOpen()
    {
        StartCoroutine(HorizontalMove_Left(Vertical_Images[0], false));
        yield return StartCoroutine(HorizontalMove_Right(Vertical_Images[1], false));
        yield return null;
    }

    private IEnumerator Anim_VerticalClose()
    {
        StartCoroutine(HorizontalMove_Right(Vertical_Images[0], true));
        yield return StartCoroutine(HorizontalMove_Left(Vertical_Images[1], true));
        yield return null;
    }
    #endregion

    #region Crack_Anim



    private IEnumerator Anim_CrackRandomFadeOn()
    {
        List<Image> TempList = new List<Image>(CrackAnim_Image_List);

        while(true)
        {
            for (int i = 0; i < 3; i++)
            {
                if (TempList.Count == 0)
                    break;

                int index = UnityEngine.Random.Range(0, TempList.Count);
                Image temp = TempList[index];
                if(TempList.Count == 1)
                    yield return StartCoroutine(FadeOnGradual(temp));
                else
                    StartCoroutine(FadeOnGradual(temp));

                TempList.RemoveAt(index);
            }
            
            
            if (TempList.Count == 0)
                break;
                

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator Anim_CrackRandomFadeOff()
    {
        List<Image> TempList = new List<Image>(CrackAnim_Image_List);

        while (true)
        {
            for (int i = 0; i < 3; i++)
            {
                if (TempList.Count == 0)
                    break;
                

                int index = UnityEngine.Random.Range(0, TempList.Count);
                Image temp = TempList[index];
                if (TempList.Count == 1)
                    yield return StartCoroutine(FadeOffGradual(temp));
                else
                    StartCoroutine(FadeOffGradual(temp));

                TempList.RemoveAt(index);
            }

            if (TempList.Count == 0)
                break;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator Anim_CrackOrderFadeOn()
    {
        List<Image> TempList = new List<Image>(CrackAnim_Image_List);
        int index = 0;

        while (true)
        {
            Image temp = TempList[index];
            StartCoroutine(FadeOnGradual(temp));
            TempList.RemoveAt(index);

            if (TempList.Count == 0)
                break;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator Anim_CrackOrderFadeOff()
    {
        List<Image> TempList = new List<Image>(CrackAnim_Image_List);
        int index = 0;

        while (true)
        {
            Image temp = TempList[index];
            StartCoroutine(FadeOffGradual(temp));
            TempList.RemoveAt(index);

            if (TempList.Count == 0)
                break;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator Anim_CrackReverseOrderFadeOn()
    {
        List<Image> TempList = new List<Image>(CrackAnim_Image_List);
        


        while (true)
        {
            int index = TempList.Count - 1;
            Image temp = TempList[index];
            StartCoroutine(FadeOnGradual(temp));
            TempList.RemoveAt(index);

            if (TempList.Count == 0)
                break;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator Anim_CrackReverseOrderFadeOff()
    {
        List<Image> TempList = new List<Image>(CrackAnim_Image_List);


        while (true)
        {
            int index = TempList.Count - 1;
            Image temp = TempList[index];
            StartCoroutine(FadeOffGradual(temp));
            TempList.RemoveAt(index);

            if (TempList.Count == 0)
                break;

            yield return new WaitForEndOfFrame();
        }

        yield return null;

    }
    #endregion
    
    #endregion

    #region Gradual_Coroutine
    private IEnumerator FadeOnGradual(Image image, float speed = 0.05f)
    {
        image.color = Color.clear;
        while (true)
        {

            if (image.color.a >= Color.black.a)
                break;
            else
            {
                Color temp = image.color;
                temp.a += speed * Anim_Speed;
                image.color = temp;
            }

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator FadeOffGradual(Image image, float speed = 0.05f)
    {
        image.color = Color.black;
        while (true)
        {

            if (image.color.a <= Color.clear.a)
                break;
            else
            {
                Color temp = image.color;
                temp.a += -speed * Anim_Speed;
                image.color = temp;
            }

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator VerticalMove_Down(Image image, bool flag)
    {
        Vector2 goal, Pos;

        image.color = Color.black;

        if (flag)
        {
            // Up to Center
            goal = image.rectTransform.anchoredPosition;
            Pos = image.rectTransform.anchoredPosition;
            Pos.y += image.rectTransform.rect.height;
            image.rectTransform.anchoredPosition = Pos;
        }
        else
        {
            // Center to Down
            goal = image.rectTransform.anchoredPosition;
            goal.y -= image.rectTransform.sizeDelta.y;
        }

        while (true)
        {
            if (image.rectTransform.anchoredPosition.y >= goal.y)
                break;

            Vector2 temp = image.rectTransform.anchoredPosition;
            temp.y += (Screen.height / Application.targetFrameRate) * Anim_Speed;
            image.rectTransform.anchoredPosition = temp;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator VerticalMove_Up(Image image, bool flag)
    {
        Vector2 goal, Pos;

        image.color = Color.black;
        if(flag)    
        {
            // Down to Center
            goal = image.rectTransform.anchoredPosition;
            Pos = image.rectTransform.anchoredPosition;
            Pos.y -= image.rectTransform.rect.height;
            image.rectTransform.anchoredPosition = Pos;
        }
        else
        {
            // Center to Up
            goal = image.rectTransform.anchoredPosition;
            goal.y += image.rectTransform.sizeDelta.y;
        }
        

        while (true)
        {
            if (image.rectTransform.anchoredPosition.y <= goal.y)
                break;

            Vector2 temp = image.rectTransform.anchoredPosition;
            temp.y -= (Screen.height / Application.targetFrameRate) * Anim_Speed;
            image.rectTransform.anchoredPosition = temp;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    private IEnumerator HorizontalMove_Right(Image image, bool flag)
    {
        Vector2 goal, Pos;

        image.color = Color.black;
        if (flag)
        {
            // Left to Center
            goal = image.rectTransform.anchoredPosition;
            Pos = image.rectTransform.anchoredPosition;
            Pos.x -= image.rectTransform.rect.width;
            image.rectTransform.anchoredPosition = Pos;
        }
        else
        {
            // Center to Right
            goal = image.rectTransform.anchoredPosition;
            goal.x += image.rectTransform.rect.width;
        }


        while (true)
        {
            if (image.rectTransform.anchoredPosition.x >= goal.x)
                break;

            Vector2 temp = image.rectTransform.anchoredPosition;
            temp.x += (Screen.width / Application.targetFrameRate) * Anim_Speed;
            image.rectTransform.anchoredPosition = temp;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }
    private IEnumerator HorizontalMove_Left(Image image, bool flag)
    {
        Vector2 goal, Pos;

        image.color = Color.black;
        if (flag)
        {
            // Right to Center
            goal = image.rectTransform.anchoredPosition;
            Pos = image.rectTransform.anchoredPosition;
            Pos.x += image.rectTransform.rect.width;
            image.rectTransform.anchoredPosition = Pos;
            Debug.Log(goal);
        }
        else
        {
            // Center to Left
            goal = image.rectTransform.anchoredPosition;
            goal.x -= image.rectTransform.rect.width;
        }


        while (true)
        {

            if (image.rectTransform.anchoredPosition.x <= goal.x)
                break;
            
            Vector2 temp = image.rectTransform.anchoredPosition;
            temp.x -= (Screen.width / Application.targetFrameRate) * Anim_Speed;
            image.rectTransform.anchoredPosition = temp;

            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    #endregion



    





}
