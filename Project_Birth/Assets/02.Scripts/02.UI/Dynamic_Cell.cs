using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dynamic_Cell : MonoBehaviour
{
    [SerializeField] private GameObject Image;
    [SerializeField, ReadOnly] private GridLayoutGroup GLG;
    [SerializeField, ReadOnly] private float Cols, Rows;

    float CellSize = 120f;
    void Awake()
    {
        GLG = GetComponent<GridLayoutGroup>();

        SetImages();
    }

    void SetImages()
    {
        float height = Camera.main.orthographicSize * 2;
        float width = height * Camera.main.aspect;
        
        Cols = Mathf.CeilToInt(width);
        Rows = Mathf.CeilToInt(height);

        for (int i = 0; i < Rows - 1; i++)
        {
            for (int j = 0; j < Cols - 1; j++)
            {
                GameObject tempobj = Instantiate(Image);
                tempobj.transform.SetParent(gameObject.transform);

                RectTransform temp = tempobj.GetComponent<RectTransform>();
                temp.localScale = new Vector3(1, 1, 1);
                temp.anchoredPosition = new Vector2(CellSize * j, -(CellSize * i));
                temp.sizeDelta = new Vector2(CellSize, CellSize);
            }
        }

        
    }
}
