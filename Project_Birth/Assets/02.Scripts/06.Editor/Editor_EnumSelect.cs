using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Event Trigger 에서 Enum형을 선택해 넘겨줄 수 있게끔 하는 클래스
/// </summary>

[RequireComponent(typeof(EventTrigger))]
public class Editor_EnumSelect : MonoBehaviour
{
    public GlobalValue.Button_Type _buttontype;

    public void InvokeButtonType(Editor_EnumSelect _type)
    {
        UIManagement._instance.OnSelectedAnimation(_type._buttontype);
    }
}
