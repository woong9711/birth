
namespace GlobalValue
{
    public enum Animation_Type
    {
        NONE,

        FadeOn,
        FadeOff,

        LtoC,
        RtoC,
        UtoC,
        DtoC,

        CtoL,
        CtoR,
        CtoU,
        CtoD,


        Crack_RandomFadeOn,
        Crack_RandomFadeOff,
        Crack_FadeOnOrder,
        Crack_FadeOffOrder,
        Crack_ReverseFadeOnOrder,
        Crack_ReverseFadeOffOrder,

        Horizontal_Open,
        Horizontal_Close,
        Vertical_Open,
        Vertical_Close,


    }

}

