namespace GlobalValue
{
    public enum ItemCode 
    {
        NONE,
        Mugwort,
        Grape,
        Garlic,
        WhiteRadish,
        Spinach 
    }

}
