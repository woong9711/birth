namespace GlobalValue
{
    public enum ListIndex
    {
        NONE = -1,
        Bear = 0,
        Human = 1,
        Rabbit = 2,
        Dog = 3,
        Wolf = 4,
        Lion = 5,
        Tiger = 6,
    }

}

