
namespace GlobalValue
{
    public enum Button_Type
    {
        NONE,
        Start,
        Start_Return,
        Shop,
        Shop_Return,
        Options,
        Options_Return,
    }
}
