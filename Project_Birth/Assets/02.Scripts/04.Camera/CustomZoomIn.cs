using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CustomZoomIn : MonoBehaviour
{
    CinemachineVirtualCamera camera;
    CinemachineTrackedDolly TrackedDolly;

    float MinOrtSize = 2.0f;
    float MaxOrtSize = 5.0f;

    float MinOffset = 0.5f;
    float MaxOffset = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Cinemachine.CinemachineVirtualCamera>();
        TrackedDolly = camera.GetCinemachineComponent<CinemachineTrackedDolly>();
    }

    void Update()
    {
        float PathPosition = TrackedDolly.m_PathPosition;
        float increase = PathPosition;
        if (PathPosition > 1.0f && PathPosition < 2.0f)
        {
            increase -= 1.0f;
            camera.m_Lens.OrthographicSize = MaxOrtSize - increase * (MaxOrtSize - MinOrtSize);
            TrackedDolly.m_PathOffset.y = MaxOffset - increase * (MaxOffset - MinOffset);
        }
        else if (PathPosition > 3.0f && PathPosition < 4.0f)
        {
            increase -= 3.0f;
            camera.m_Lens.OrthographicSize = MinOrtSize + increase * (MaxOrtSize - MinOrtSize);
            TrackedDolly.m_PathOffset.y = MinOffset + increase * (MaxOffset - MinOffset);
        }
        else if (PathPosition > 5.0f && PathPosition < 6.0f)
        {
            increase -= 5.0f;
            camera.m_Lens.OrthographicSize = MaxOrtSize - increase * (MaxOrtSize - MinOrtSize);
            TrackedDolly.m_PathOffset.y = MaxOffset - increase * (MaxOffset - MinOffset);
        }
        else if (PathPosition > 7.0f && PathPosition < 8.0f)
        {
            increase -= 7.0f;
            camera.m_Lens.OrthographicSize = MinOrtSize + increase * (MaxOrtSize - MinOrtSize);
            TrackedDolly.m_PathOffset.y = MinOffset + increase * (MaxOffset - MinOffset);
        }

    }

}
