using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Trigger_Passway : MonoBehaviour
{
    bool flag = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Trigger!");
            if(Player_TriggerReceiver._instance.StatueFlag)
            {
                StartCoroutine(DownTrigger());
                Player_TriggerReceiver._instance.CurPhase = 1;
            }
                
        }
    }


    IEnumerator DownTrigger()
    {
        if (flag)
            yield return null;

        yield return new WaitForSeconds(2.0f);
        for (int i = 0; i < Player_TriggerReceiver._instance.WallList.Count; i++)
        {
            StartCoroutine(Player_TriggerReceiver._instance.WallList[i].OnAction());
            yield return new WaitForSeconds(0.5f);
        }

    }
}
