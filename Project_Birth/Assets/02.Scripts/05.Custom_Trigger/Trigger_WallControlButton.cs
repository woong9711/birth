using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Trigger_WallControlButton : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Trigger!");
            Player_TriggerReceiver._instance.State = "WallControlButton";
            int temp = int.Parse(gameObject.name.Split('_')[2]);
            Player_TriggerReceiver._instance.CurButtonIndex = temp;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Trigger Exit!");
            Player_TriggerReceiver._instance.State = "None";
            Player_TriggerReceiver._instance.CurButtonIndex = -1;
        }
    }
}
