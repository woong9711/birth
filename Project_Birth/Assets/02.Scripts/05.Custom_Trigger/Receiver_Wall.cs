using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Receiver_Wall : MonoBehaviour
{
    [SerializeField, ReadOnly] public bool Flag; // Down -> true, Up -> false
    public float speed;
    public Vector2 GoalVector;

    private Vector2 Ori_Pos;

    bool animated = false;

    private void Start()
    {
        Ori_Pos = gameObject.transform.position;
        Flag = true;
    }

    public IEnumerator OnAction()
    {
        if (animated)
            yield return null;
        if (Flag)
        {
            // Drop Down
            yield return StartCoroutine(ToDown());
        }
        else
        {
            // Up
            yield return StartCoroutine(ToUp());
        }
        Flag = !Flag;
    }

    IEnumerator ToDown()
    {
        animated = true;
        while (true)
        {
            Vector2 pos = gameObject.transform.position;
            if (pos.y <= GoalVector.y)
                break;

            pos += Vector2.down * speed * Time.deltaTime;
            gameObject.transform.position = pos;
            yield return null;
        }
        animated = false;
    }

    IEnumerator ToUp()
    {
        animated = true;
        while (true)
        {
            Vector2 pos = gameObject.transform.position;
            if (pos.y >= Ori_Pos.y)
                break;

            pos += Vector2.up * speed * Time.deltaTime;
            gameObject.transform.position = pos;
            yield return null;
        }
        animated = false;
    }
}
