using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Trigger_Desk : MonoBehaviour
{
    public GameObject Item;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            Debug.LogFormat("Trigger! : {0}", Item.name);

            Player_TriggerReceiver._instance.State = "Desk";
            Player_TriggerReceiver._instance.CurTrigger_Item = Item;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.LogFormat("Trigger Exit! : {0}", Item.name);

            Player_TriggerReceiver._instance.State = "None";
            Player_TriggerReceiver._instance.CurTrigger_Item = null;
        }
    }
}
