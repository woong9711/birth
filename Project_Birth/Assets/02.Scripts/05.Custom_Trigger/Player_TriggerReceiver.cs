using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_TriggerReceiver : MonoBehaviour
{
    static private Player_TriggerReceiver Instance;
    static public Player_TriggerReceiver _instance
    {
        get
        {
            if (Instance == null)
                return null;
            return Instance;
        }
    }

    [ReadOnly] public bool MoveFlag;
    [ReadOnly] public bool StatueFlag;
    [ReadOnly] public string State;
    [ReadOnly] public GameObject CurTrigger_Item;
    [ReadOnly] public int CurButtonIndex;
    [ReadOnly] public int CurPhase;

    [SerializeField, ReadOnly] private RectTransform SelectItemUI;

    public List<Receiver_Wall> WallList;
    public List<GameObject> Statue_List;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        FindObject();
    }

    private void Start()
    {
        MoveFlag = true;
        StatueFlag = false;
        CurButtonIndex = -1;
        State = "None";
        CurPhase = -1;

        

    }

    void FindObject()
    {
        SelectItemUI = GameObject.Find("SelectItemUI").GetComponent<RectTransform>();
    }

    public void OnAction()
    {
        switch (State)
        {
            case "None":
                Debug.Log("Not Working");
                break;
            case "Desk":
                if(CurTrigger_Item.activeSelf)
                {
                    Inventory._instance.AddItem(CurTrigger_Item);
                    CurTrigger_Item.SetActive(false);
                }
                else
                {
                    Inventory._instance.RemoveItem(CurTrigger_Item);
                    CurTrigger_Item.SetActive(true);
                }
                
                break;
            case "Statue":
                if(Inventory._instance.IsEmpty())
                {
                    Debug.Log("Not Working");
                    break;
                }

                Inventory._instance.gameObject.SetActive(false);

                SelectItemUI.gameObject.SetActive(true);
                SelectItemUI.anchoredPosition = Vector2.zero;
                SelectItemUI temp = SelectItemUI.gameObject.GetComponent<SelectItemUI>();
                if (temp != null)
                {
                    List<Image> listtemp = Inventory._instance.GetItemImages();
                    temp.SetItemSprite(listtemp);
                }
                break;
            case "WallControlButton":

                // 3번방
                switch (CurPhase)
                {
                    case 1:
                        StartCoroutine(Phase_01()); // 1페이즈
                        break;
                    case 2:
                        StartCoroutine(Phase_02()); // 2페이즈
                        break;
                    case 3:
                        StartCoroutine(Phase_03()); // 3페이즈
                        break;
                    case 4:
                        StartCoroutine(Phase_04()); // 4페이즈
                        break;
                    case 5:
                        StartCoroutine(Phase_05()); // 5페이즈
                        break;
                    case 6:
                        StartCoroutine(Phase_06()); // 6페이즈
                        break;
                }

                break;
        }
    }

    IEnumerator Run_Animation()
    {
        yield return new WaitForSeconds(1.0f);
    }

    IEnumerator Phase_01()
    {
        yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
        switch (CurButtonIndex)
        {
            case 1:
                // 1번 가림막 올라가고 토끼석상 부서진다. 그후 페이즈 종료
                Statue_List[(int)GlobalValue.ListIndex.Rabbit].SetActive(false);
                yield return StartCoroutine(Run_Animation());
                ++CurPhase;
                break;
            case 2:
                // 2번 가림막 올라갔다가 개랑 토끼 추격 애니메이션 후 가림막 내려간다.
                yield return StartCoroutine(Run_Animation());
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 3:
                // 3번 가림막 올라갔다가 늑대가 개에게 달려들고 게임오버 후 처음 3번방 입장으로 돌아간다.
                yield return StartCoroutine(Run_Animation());
                break;
            case 4:
                // 4번 가림막 올라갔다가 사자가 늑대에게 달려들고 게임오버 후 처음 3번방 입장으로 돌아간다.
                yield return StartCoroutine(Run_Animation());
                break;
            case 5:
                // 5번 가림막 올라갔다가 호랑이와 사자는 가만히 있다가 가림막 내려간다.
                yield return StartCoroutine(Run_Animation());
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
        }

        yield return null;
    }
    IEnumerator Phase_02()
    {
        yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
        switch (CurButtonIndex)
        {
            case 1:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 2:
                // 개가 기존 토끼 위치로 이동후 사람에게 길들여지는 모션후 페이즈 종료
                Statue_List[(int)GlobalValue.ListIndex.Dog].transform.position = Statue_List[(int)GlobalValue.ListIndex.Rabbit].transform.position;
                yield return StartCoroutine(Run_Animation());
                ++CurPhase;
                break;
            case 3:
                // 3번 가림막 올라갔다가 늑대가 개에게 달려들고 게임오버 후 처음 3번방 입장으로 돌아간다.
                Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position = Statue_List[(int)GlobalValue.ListIndex.Dog].transform.position;
                yield return StartCoroutine(Run_Animation());
                break;
            case 4:
                // 4번 가림막 올라갔다가 사자가 늑대에게 달려들고 게임오버 후 처음 3번방 입장으로 돌아간다.
                Statue_List[(int)GlobalValue.ListIndex.Lion].transform.position = Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position;
                yield return StartCoroutine(Run_Animation());
                break;
            case 5:
                // 5번 가림막 올라갔다가 호랑이와 사자는 가만히 있다가 가림막 내려간다.
                yield return StartCoroutine(Run_Animation());
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
        }
        yield return null;
    }
    IEnumerator Phase_03()
    {
        yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
        switch (CurButtonIndex)
        {
            case 1:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 2:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 3:
                // 인간과 개가 동시에 늑대에게 달려들고 늑대석상 파괴 후 페이즈 종료
                Statue_List[(int)GlobalValue.ListIndex.Human].transform.position = Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position;
                Statue_List[(int)GlobalValue.ListIndex.Dog].transform.position = Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position;

                Statue_List[(int)GlobalValue.ListIndex.Wolf].SetActive(false);
                yield return StartCoroutine(Run_Animation());
                ++CurPhase;
                break;
            case 4:
                // 4번 가림막 올라갔다가 사자가 늑대에게 달려들고 게임오버 후 처음 3번방 입장으로 돌아간다.
                Statue_List[(int)GlobalValue.ListIndex.Lion].transform.position = Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position;
                yield return StartCoroutine(Run_Animation());
                break;
            case 5:
                // 5번 가림막 올라갔다가 호랑이와 사자는 가만히 있다가 가림막 내려간다.
                yield return StartCoroutine(Run_Animation());
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
        }
        yield return null;

    }
    IEnumerator Phase_04()
    {
        yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
        switch (CurButtonIndex)
        {
            case 1:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 2:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 3:
                // 아무일도 일어나지 않는다. 하지만 가림막이 올라가지 않는다. 그후 페이즈 종료
                ++CurPhase;
                break;
            case 4:
                // 4번 가림막 올라갔다가 사자가 늑대에게 달려들고 게임오버 후 처음 3번방 입장으로 돌아간다.
                Statue_List[(int)GlobalValue.ListIndex.Lion].transform.position = Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position;
                yield return StartCoroutine(Run_Animation());
                break;
            case 5:
                // 5번 가림막 올라갔다가 호랑이와 사자는 가만히 있다가 가림막 내려간다.
                yield return StartCoroutine(Run_Animation());
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
        }
        yield return null;
    }

    IEnumerator Phase_05()
    {
        yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
        switch (CurButtonIndex)
        {
            case 1:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 2:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 3:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 4:
                // 4번 가림막 올라간후 사자가 늑대위에 서고 크게 표효한다. 게임오버 후 처음 3번방 입장으로 돌아간다.
                Statue_List[(int)GlobalValue.ListIndex.Lion].transform.position = Statue_List[(int)GlobalValue.ListIndex.Wolf].transform.position;
                yield return StartCoroutine(Run_Animation());
                break;
            case 5:
                // 5번 가림막 올라갔다가 호랑이와 사자가 서로 바라보며 가만히 있는다. 하지만 가림막이 다시 내려가지 않는다. 그후 페이즈 종료
                yield return StartCoroutine(Run_Animation());
                ++CurPhase;
                break;
        }
        yield return null;
    }
    IEnumerator Phase_06()
    {
        yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
        switch (CurButtonIndex)
        {
            case 1:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 2:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 3:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
            case 4:
                // 4번 가림막이 올라가고 사자와 호랑이 석상은 늑대 석상 방향으로 이동후 둘끼리 치고받고 싸운다. 그후 두 석상 모두 파괴된다. 그후 페이즈 종료
                yield return StartCoroutine(Run_Animation());
                Statue_List[(int)GlobalValue.ListIndex.Lion].SetActive(false);
                Statue_List[(int)GlobalValue.ListIndex.Tiger].SetActive(false);
                ++CurPhase;
                break;
            case 5:
                // 아무일도 일어나지 않는다.
                yield return StartCoroutine(WallList[CurButtonIndex - 1].OnAction());
                break;
        }
        yield return null;
    }
}
