using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public float jumpPower;

    public Rigidbody2D rigidbody2D;

    Vector2 MoveVector;
    void Start()
    {
        MoveVector = Vector2.zero;
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private bool touched;

    // Update is called once per frame
    void Update()
    {
        if (Player_TriggerReceiver._instance.MoveFlag)
        {
            if(!touched)
                KeyboardMove();

            Vector2 pos = transform.position;
            pos += MoveVector * speed * Time.deltaTime;
            transform.position = pos;
        }
    }

    void KeyboardMove()
    {
        if (Input.GetKey(KeyCode.A))
            MoveVector.x = -1;
        else if (Input.GetKey(KeyCode.D))
            MoveVector.x = 1;
        else
            MoveVector.x = 0;

        if (Input.GetKeyDown(KeyCode.F))
        {
            Player_TriggerReceiver temp = GetComponent<Player_TriggerReceiver>();
            temp.OnAction();
        }

        if (Input.GetKeyDown(KeyCode.Space))
            rigidbody2D.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
    }

    public void OnPointerDown_LeftArrows()
    {
        MoveVector.x = -1;
        touched = true;
    }
    public void OnPointerDown_RightArrows()
    {
        MoveVector.x = 1;
        touched = true;
    }
    public void OnPointerDown_Jump()
    {
        rigidbody2D.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        touched = true;
    }
    public void OnPointerUp_Jump()
    {
        touched = false;
    }

    public void OnPointerUp()
    {
        MoveVector.x = 0;
        touched = false;
    }

}
