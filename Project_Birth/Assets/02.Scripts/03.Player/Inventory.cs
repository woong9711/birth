using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    static private Inventory Instance;
    static public Inventory _instance
    {
        get
        {
            if (Instance == null)
                return null;
            return Instance;
        }
    }

    [ReadOnly] public List<GameObject> ItemList;
    [SerializeField, ReadOnly] private int curItemCount = 0;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Start()
    {
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            ItemList.Add(gameObject.transform.GetChild(i).gameObject);
            ItemList[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddItem(GameObject item)
    {
        ItemList[curItemCount].SetActive(true);
        Image tempImage = ItemList[curItemCount].GetComponent<Image>();
        Editor_ItemElement tempElement = ItemList[curItemCount].GetComponent<Editor_ItemElement>();
        if (tempImage != null && tempElement != null)
        {
            tempImage.sprite = item.GetComponent<SpriteRenderer>().sprite;
            tempElement.code = item.GetComponent<Editor_ItemElement>().code;
        }

        ++curItemCount;
    }

    public void RemoveItem(GameObject item)
    {
        int index = -1;
        Editor_ItemElement tempparaElement = item.GetComponent<Editor_ItemElement>();
        for (int i = 0; i < curItemCount; i++)
        {
            Editor_ItemElement tempElement = ItemList[i].GetComponent<Editor_ItemElement>();
            if (tempparaElement.code == tempElement.code)
            {
                Image tempImage = ItemList[i].GetComponent<Image>();
                tempImage.sprite = null;
                tempElement.code = GlobalValue.ItemCode.NONE;

                index = i;
                --curItemCount;
                break;
            }
        }

        if(index != -1)
            SortInventory(index);

    }

    public GameObject FindwithCode(GlobalValue.ItemCode code)
    {
        foreach (var item in ItemList)
        {
            GlobalValue.ItemCode temp = item.GetComponent<Editor_ItemElement>().code;
            if (temp == code)
                return item;
        }

        return null;
    }

    void SortInventory(int index)
    {
        GameObject temp = ItemList[index];
        ItemList.RemoveAt(index);
        ItemList.Add(temp);

        ItemList[ItemList.Count - 1].transform.SetAsLastSibling();
        ItemList[ItemList.Count - 1].SetActive(false);
    }

    public bool IsEmpty()
    {
        if (curItemCount == 0)
            return true;
        return false;
    }

    public List<Image> GetItemImages()
    {
        List<Image> temp = new List<Image>();

        for (int i = 0; i < curItemCount; i++)
        {
            temp.Add(ItemList[i].GetComponent<Image>());
        }


        return temp;
    }

}

