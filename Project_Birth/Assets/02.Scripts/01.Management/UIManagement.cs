using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagement : MonoBehaviour
{
    static private UIManagement Instance;
    static public UIManagement _instance
    {
        get
        {
            if (Instance == null)
                return null;
            return Instance;
        }
    }

    [Header("Animation Image")]
    [SerializeField] private Scene_Turn_Animations Controller;
    public Scene_Turn_Animations _controller
    {
        get;  set;
    }

    [Header("Animations List")]
    [SerializeField] private List<UI_Animations> AnimationList = new List<UI_Animations>();
    public List<UI_Animations> _animationList
    {
        get;
    }

    private UI_Animations CurAnim;

    public void OnSelectedAnimation(GlobalValue.Button_Type _btype)
    {
        foreach (var item in AnimationList)
        {
            if (item._btype == _btype)
            {
                Controller.SetUIAnimation(item);
                CurAnim = item;
                return;
            }
        }
        
    }

    void Convert_SceneName()
    {
        List<string> SceneList = new List<string>();

        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            string temp = System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
            SceneList.Add(temp.Split('.')[1]);
        }


        for (int i = 0; i < AnimationList.Count; i++)
        {
            if(SceneList.Contains(AnimationList[i].SceneName))
            {
                string temp = string.Format("{0:D2}.{1}", SceneList.IndexOf(AnimationList[i].SceneName), AnimationList[i].SceneName);
                AnimationList[i].SceneName = temp;
            }
        }

    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    /// <summary>
    /// Call Load Scene After
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="level"></param>
    void OnSceneLoaded(Scene scene, LoadSceneMode level)
    {
        if (CurAnim == null)
            return;
        
        Controller.SetUIAnimation(CurAnim, false);
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        Convert_SceneName();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
